#TODO: Create Package Includer

#!/usr/bin/python
import urllib2
import cgi
import webapp2
import json
from google.appengine.ext import ndb
from google.appengine.ext.ndb import query
# - Custom Modules
import DataPopulationScripts
import Permissions
import ReqHandle_Permission
import ReqHandle_User
from User import User
from datetime import date

class main(webapp2.RequestHandler):
    def get(self):
        self.response.out.write("<html><head>")
        self.response.out.write("<strong>Welcome to the Remote Health Web Service!</strong>")
        self.response.out.write("</head><body>")
        self.response.out.write("<p>Input your desired URL to populate data or display return formats</p>")
        self.response.out.write("</body></html>")

class Login_Protocol(webapp2.RequestHandler):
    def get(self):
        req_email = self.request.headers['login_email']
        req_password = self.request.headers['login_password']
        _ukey = ndb.Key('User', req_email)
        _user = _ukey.get()

        if _user != None:
            if _user.password == req_password:
                user_dict = _user.logindetails_userdict()
                userper_list = []
                for cur_perm in _user.permissions:
                    userper_list.append(cur_perm.to_dict())

                user_dict['Permissions'] = userper_list
                self.response.write(json.dumps(user_dict))
        else:
            self.response.write(json.dumps({}))

application = webapp2.WSGIApplication([
        # Main functions
        ("/", main),
        ("/logprot", Login_Protocol),

        # DataPopulationScripts Functions
        ("/dini/perm", DataPopulationScripts.script_Permission),
        ("/dini/doc", DataPopulationScripts.script_Doctor),
        ("/dini/nur", DataPopulationScripts.script_Nurse),
        ("/dini/pat", DataPopulationScripts.script_Patients),
        ("/dini/disdoc", DataPopulationScripts.doctor_display),
        ("/dini/disnur", DataPopulationScripts.nurse_display),
        ("/dini/dispat", DataPopulationScripts.patient_display),
        ("/dini/disupat", DataPopulationScripts.upatient_display),
        ("/dini/logind", DataPopulationScripts.login_display),
        ("/dini/pinf", DataPopulationScripts.patient_info_display),
        ("/dini/dut", DataPopulationScripts.delete_user_test),
        ("/dini/create_tasks", DataPopulationScripts.Create_Task_Test),

        # ReqHandle_Permission Functions
        ("/perm/ret_perm", ReqHandle_Permission.Retrieve_Permission_List),          # implemented
        ("/rh_perm/ret_uperms", ReqHandle_Permission.Retrieve_Users_Permissions),   # implemented
        ("/rh_perm/ch_perm", ReqHandle_Permission.Change_Permissions),              # implemented
        ("/rh_perm/cr_task", ReqHandle_Permission.Create_Task),                     # implemented
        ("/rh_perm/ret_task", ReqHandle_Permission.Retrieve_TaskList),              # implemented
        ("/rh_perm/comp_task", ReqHandle_Permission.Complete_Task),                 # implemented
        ("/rh_perm/cr_unote", ReqHandle_Permission.Create_UserNote),                # Implemented
        ("/rh_perm/ret_unote", ReqHandle_Permission.Retrieve_UserNote),             # implemented

        #ReqHandle_User Request Handlers
        ("/rh_user/cr_user", ReqHandle_User.Create_User),
        ("/rh_user/ch_user", ReqHandle_User.Change_User_Details),
        ("/rh_user/del_user", ReqHandle_User.Delete_User),
        ("/rh_user/ret_contlist", ReqHandle_User.Retrieve_ContactList),
        ("/rh_user/ch_logcred", ReqHandle_User.Change_LoginCredentials),

        ], debug=True)




# - Sublime Color Prefs
# - #FF9900 ora
# - #6230AC perp
# - #0C0C0C dark grey Back
