import urllib2
import webapp2
import json
from google.appengine.ext import ndb
from google.appengine.ext.ndb import query
# - Custom Modules -
from Permissions import Permission
from Permissions import Task
from Permissions import User_Note
import User
from datetime import datetime


def DEFAULT_DOCTOR():
	plist = Permission.query()
	permission_list = []
	for p in plist:
		if p.alias != 'Debug_Console':
			permission_list.append(p)
	return permission_list

def DEFAULT_NURSE():
	pquery = Permission.query()
	permission_list = []
	for p in pquery:
		if p.alias == 'Location':
			permission_list.append(p)
		elif p.alias == 'Conference':
			permission_list.append(p)
		elif p.alias == 'Messaging':
			permission_list.append(p)
		elif p.alias == 'User_Note':
			permission_list.append(p)
		elif p.alias == 'Contact_List':
			permission_list.append(p)
	return permission_list

def DEFAULT_PATIENT():
	pl = Permission.query()
	permission_list = []
	for p in pl:
		if p.alias == 'Assist':
			permission_list.append(p)
	return permission_list

# - Permission Related Handlers
class Retrieve_Permission_List(webapp2.RequestHandler):
	def get(self):
		permissions = Permission.query()
		perm_list = []
		for cur_perm in permissions:
			perm_list.append(cur_perm.to_dict())
		self.response.write(json.dumps(perm_list))

class Retrieve_Users_Permissions(webapp2.RequestHandler):
	def get(self):
# - Get User based of key, then fucking deal drugs with it
		user_key = User.Construct_User_Key(self.request.headers['email'])
		_user = user_key.get()
		perm_list = []
		if _user != None:
			for cur_perm in _user.Permissions:
				perm_list.append(cur_perm.to_dict())
		self.response.write(json.dumps(perm_list))


# - TODO: Test Changing Permissions
class Change_Permissions(webapp2.RequestHandler):
	def post(self):
		user_key = User.Construct_User_Key(self.request.headers['email'])
		_user = user_key.get()
		if _user != None:
			change_list = self.request.headers['permissions']
			if change_list != None:
				_user.permissions = change_list
			_user.put()



# - Task Related Permissions
class Create_Task(webapp2.RequestHandler):
	def post(self):
		_assigned_email = self.request.headers['assigned_email']
		_assigned_id = User.Construct_User_Key(self.request.headers['assigned_email'])
		_patient = self.request.headers['patient_email']
		_creator = self.request.headers['creator_email']
		_category = self.request.headers['category']
		_task_info = self.request.headers['task_info']
		task = Task(assigned_id = _assigned_id,
					assigned_email = _assigned_email,
					patient_id = _patient,
					creator_id = _creator,
					task_info = _task_info)
		task.put()

class Retrieve_TaskList(webapp2.RequestHandler):
	def get(self):
		user_key = User.Construct_User_Key(self.request.headers['email'])
		# Tester: user_key = User.Construct_User_Key('ereid@hospital')
		tasks = Task.query(Task.assigned_id == user_key).order(Task.date_assigned, -Task.date_assigned)
		task_list = []
		for t in tasks:
			task_list.append(t.to_dict())
		self.response.write(json.dumps(task_list))

class Complete_Task(webapp2.RequestHandler):
	def get(self):
		user_key = User.Construct_User_Key(self.request.headers['email'])
		cr_date = datetime(self.request.headers['date_assigned'])
		taskc = Task.query(ndb.AND 	(Task.assigned_id == user_key),
									(Task.patient_id == self.request.headers['patient_email']),
									(Task.creator_id == self.request.headers['creator_email']),
									(Task.category == self.request.headers['category'])
									(Task.date_assigned == cr_date))
		if taskc.count() == 1:
			for t in taskc:
				t.task_status = 'COMPLETED'
				t.put()


# - User Note Related Permissions
class Create_UserNote(webapp2.RequestHandler):
	def post(self):
		_assigned_id = User.Construct_User_Key(self.request.headers['assigned_email'])
		_author_id = self.request.headers['author_email']
		_note = self.request.headers['user_note']
		user_note = User_Note(	assigned_id = _assigned_id,
								author_id = _author_id,
								note = _user_note)
		user_note.put()

#TODO: Need to check if Getting UserNotes Works
class Retrieve_UserNote(webapp2.RequestHandler):
	def get(self):
		req_key = User.Construct_User_Key(self.request.headers['email'])
		user_notes = User_Note.query(UserNote.assigned_id == req_key)
		note_list = []
		for un in user_notes:
			note_list.append(un.to_dict())
		self.response.write(json.dumps(note_list))
