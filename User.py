from google.appengine.ext import ndb
from google.appengine.ext.ndb import query
from datetime import date
from Permissions import Permission

class User(ndb.Model):
    first_name = ndb.StringProperty(required = True)
    surname = ndb.StringProperty(required = True)
    email = ndb.StringProperty(indexed = True, required = True)
    address = ndb.StringProperty()
    password = ndb.StringProperty(default = 'qwerty')
    account_type = ndb.StringProperty(required = True, choices=['Doctor', 'Nurse', 'Patient', 'User'])
    current_location = ndb.GeoPtProperty()
    permissions = ndb.StructuredProperty(Permission, repeated = True)


# - Local function defs for User Class
    def logindetails_userdict(self):
        user_dict = {   'first_name': self.first_name,
                        'surname': self.surname,
                        'email' : self.email,
                        'account_type' : self.account_type,
                        'Permissions' : [],
                        'address' : self.address }

        return user_dict

# - contact list login details
    def contactlist_userdict(self):
        user_dict = {   'first_name': self.first_name,
                        'surname': self.surname,
                        'email' : self.email,
                        'account_type' : self.account_type,
                        'availability_status' : 'OFFLINE',
                        'current_location' : self.current_location,
                        'address' : self.address }
        return user_dict

def Construct_User_Key(key_value):
    return ndb.Key('User', key_value)

# - Patient subclass to user class.
class Patient_Info(ndb.Model):
    patient = ndb.KeyProperty(kind=User, required = True)
    email = ndb.StringProperty()
    dob = ndb.DateProperty(required = True)
    age = ndb.IntegerProperty()
    gender = ndb.StringProperty(choices=['Male', 'Female'])
    height = ndb.IntegerProperty()
    weight = ndb.IntegerProperty()
    bmi = ndb.IntegerProperty()
    condition_status = ndb.StringProperty(choices = ['LOW', 'MEDIUM', 'HIGH', 'CRITICAL'], default = 'LOW')

    def to_dict(self):
        datstr = str(self.dob)
        patient_dict = {'dob' : datstr,
                        'age' : self.age,
                        'gender' : self.gender,
                        'height' : self.height,
                        'weight' : self.weight,
                        'bmi' : self.bmi,
                        'condition_status' : self.condition_status }
        return patient_dict

class Archived_Info(ndb.Model):
    archived_date = ndb.DateTimeProperty(auto_now_add = True)
    patient = ndb.KeyProperty(required = True)
    email = ndb.StringProperty(required = True)
    dob = ndb.DateProperty(required = True)
    gender = ndb.StringProperty(choices=['Male', 'Female'])
    height = ndb.IntegerProperty()
    weight = ndb.IntegerProperty()
    bmi = ndb.IntegerProperty()
    condition_status = ndb.StringProperty(choices = ['LOW', 'MEDIUM', 'HIGH', 'CRITICAL'], default = 'LOW')
