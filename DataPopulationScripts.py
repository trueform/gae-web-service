import urllib2
import webapp2
from google.appengine.ext import ndb
from google.appengine.ext.ndb import query
import json
from Permissions import Permission
from Permissions import Task
from User import User
from User import Construct_User_Key
from User import Patient_Info
from datetime import date


class script_Permission(webapp2.RequestHandler):
    def get(self):
        # - Validating Permissions are empty. This should only be executred once per
        # - instance, as keys are not implemented for permissions.
        if Permission.query().count() == 0:
            Location = Permission(alias='Location')
            Location.put()
            Task_Assignment = Permission(alias='Task_Assignment')
            Task_Assignment.put()
            Conference = Permission(alias='Conference')
            Conference.put()
            Messaging = Permission(alias='Messaging')
            Messaging.put()
            User_Note = Permission(alias='User_Note')
            User_Note.put()
            Contact_List = Permission(alias='Contact_List')
            Contact_List.put()
            Account_Management = Permission(alias='Account_Management')
            Account_Management.put()
            File_Transfer = Permission(alias='File_Transfer')
            File_Transfer.put()
            Assist = Permission(alias='Assist')
            Assist.put()
            # - Admin : Literally hardcoded to only test users atm.
            Debug_Console = Permission(alias='Debug_Console')
            Debug_Console.put()

class script_Doctor(webapp2.RequestHandler):
    def get(self):
        if User.query(User.account_type == 'Doctor').count() == 0:
            pl = Permission.query()
            deb = Permission()
            permission_list = []
            if pl.count() > 0:
                for p in pl:
                    if p.alias == 'Debug_Console':
                        deb = p
                    else:
                        permission_list.append(p)

            # - Doctor 1
            d1_key = ndb.Key('User', 'jdorian@hospital')
            doctor_1 = User(key=d1_key,
                            first_name = 'John',
                            surname = 'Dorian',
                            email = 'jdorian@hospital',
                            address = '2/12 Oakley Drive',
                            account_type = 'Doctor',
                            permissions = permission_list)

            doctor_1.put()

            # - Doctor 2
            d2_key = ndb.Key('User', 'pcox@hospital')
            doctor_2 = User(key = d2_key,
                            first_name = 'Perry',
                            surname = 'Cox',
                            email = 'pcox@hospital',
                            address = '712 Luxury Lane',
                            account_type = 'Doctor',
                            permissions = permission_list)
            doctor_2.put()
            # - Doctor 3
            d3_key = ndb.Key('User', 'cturk@hospital')
            doctor_3 = User(key = d3_key,
                            first_name = 'Chris',
                            surname = 'Turk',
                            email = 'cturk@hospital',
                            address = '42 Hitchiker Highway',
                            account_type = 'Doctor',
                            permissions = permission_list)
            doctor_3.put()
            # - Test Doctor
            permission_list.append(deb)
            tkey = ndb.Key('User', 'd')
            test_doc = User(key = tkey,
                            first_name = 'Test',
                            surname = 'Doctor',
                            email = 'd',
                            address = '2 Noob Way',
                            password = 'd',
                            account_type = 'Doctor',
                            permissions = permission_list)
            test_doc.put()

class script_Nurse(webapp2.RequestHandler):
    def get(self):
        if User.query(User.account_type == 'Nurse').count() == 0:
            pl = Permission.query()
            permission_list = []
            deb = Permission()
            pl = Permission.query()
            for p in pl:
                if p.alias == 'Location':
                    permission_list.append(p)
                elif p.alias == 'Conference':
                    permission_list.append(p)
                elif p.alias == 'Messaging':
                    permission_list.append(p)
                elif p.alias == 'User_Note':
                    permission_list.append(p)
                elif p.alias == 'Contact_List':
                    permission_list.append(p)
                elif p.alias == 'Debug_Console':
                    deb = p

            # - Nurse 1
            n1key = Construct_User_Key('cespinosa@hospital')
            nurse_1 = User( key = n1key,
                            first_name = 'Carla',
                            surname = 'Espinosa',
                            email = 'cespinosa@hospital',
                            address = '42 Hitchiker Highway',
                            account_type = 'Nurse',
                            permissions = permission_list)
            nurse_1.put()
            # - Nurse 2
            n2key = Construct_User_Key('lroberts@hospital')
            nurse_2 = User( key = n2key,
                            first_name = 'Laverne',
                            surname = 'Roberts',
                            email = 'lroberts@hospital',
                            address = '4 Bucket-oChicken Street',
                            account_type = 'Nurse',
                            permissions = permission_list)
            nurse_2.put()
            # - Nurse 3
            n3key = Construct_User_Key('ereid@hospital')
            nurse_3 = User(key = n3key,
                            first_name = 'Elliot',
                            surname = 'Reid',
                            email = 'ereid@hospital',
                            address = '69 Blonde Fool drive',
                            account_type = 'Nurse',
                            permissions = permission_list)
            nurse_3.put()
            # - Test Nurse
            permission_list.append(deb)
            tkey = Construct_User_Key('n')
            test_nurse = User(key = tkey,
                            first_name = 'Test',
                            surname = 'Nurse',
                            email = 'n',
                            address = '1 Noob Way',
                            password = 'n',
                            account_type = 'Nurse',
                            permissions = permission_list)
            test_nurse.put()

class script_Patients(webapp2.RequestHandler):
    def get(self):

        # - Construct the default permission list
        if User.query(User.account_type == 'Patient').count() == 0:
            perm_list = []
            deb = Permission()
            pl = Permission.query()
            for p in pl:
                if p.alias == 'Assist':
                    perm_list.append(p)
                if p.alias == 'Debug_Console':
                    deb = p


            # - Patient 1
            p1key = Construct_User_Key('rbobby@external')
            patient_1 = User(key=p1key,
                            first_name = 'Ricky',
                            surname = 'Bobby',
                            email = 'rbobby@external',
                            address = '2234 Rich Street',
                            account_type = 'Patient',
                            permissions = perm_list)
            patient_1.put()

            p1Info = Patient_Info(patient = patient_1.key,
                                email = 'rbobby@external',
                                dob = date(1664, 5, 20),
                                gender = 'Male',
                                height = 173,
                                weight = 98)
            p1Info.put()

            # - Patient 2
            p2key = Construct_User_Key('cjunior@external')
            patient_2 = User(key = p2key,
                            first_name = 'Cal-Norton',
                            surname = 'Junior',
                            email = 'cjunior@external',
                            address = '123123 Galaxy West',
                            account_type = 'Patient',
                            permissions = perm_list)
            patient_2.put()

            p2Info = Patient_Info(patient = patient_2.key,
                                email = 'cjunior@external',
                                dob = date(1975, 12, 15),
                                gender = 'Male',
                                height = 180,
                                weight = 150,
                                condition_status = 'CRITICAL')
            p2Info.put()

            # - Patient 3
            p3key = Construct_User_Key('lwang@external')
            patient_3 = User(key = p3key,
                            first_name = 'Lou',
                            surname = 'Wang',
                            email = 'lwang@external',
                            address = '24 DimSim Way',
                            account_type = 'Patient',
                            permissions = perm_list)
            patient_3.put()
            p3Info = Patient_Info(patient = patient_3.key,
                            dob = date(2000, 1, 1),
                            email = 'lwang@external',
                            gender = 'Female',
                            height = 121,
                            weight = 35,
                            condition_status = 'MEDIUM')
            p3Info.put()


            # - Test Patient
            perm_list.append(deb)

            tpkey = Construct_User_Key('p')
            test_patient = User(key = tpkey,
                            first_name = 'Test',
                            surname = 'Patient',
                            email = 'p',
                            address = '3 Noob Way',
                            password = 'p',
                            account_type = 'Patient',
                            permissions = perm_list)
            test_patient.put()

            tpInfo = Patient_Info(patient = test_patient.key,
                            dob = date(2012, 5, 5),
                            email = 'p',
                            gender = 'Male',
                            height = 200,
                            weight = 300,
                            condition_status = 'HIGH')
            tpInfo.put()

class Create_Task_Test(webapp2.RequestHandler):
    def get(self):
        # - Creating tasks
        t1key = Construct_User_Key('ereid@hospital')
        task_1 = Task(  assigned_id = t1key,
                        category = 'Test Category',
                        assigned_email = 'ereid@hosphospital',
                        patient_id = 'p',
                        creator_id = 'd',
                        task_info = 'This is the first Test Task')
        task_1.put()

        task_2 = Task( assigned_id = t1key,
                        category = 'Test Category',
                        assigned_email = 'ereid@hosphospital',
                        patient_id = 'lwang@external',
                        creator_id = 'd',
                        task_info = 'This is the second test Task')
        task_2.put()

        task_3 = Task(  assigned_id = t1key,
                        category = 'Test Category',
                        assigned_email = 'ereid@hospital',
                        patient_id = 'cjunior@external',
                        creator_id = 'd',
                        task_info = 'This is the third test task')
        task_3.put()
# ---------------------------------------
# - Display Methods
# ---------------------------------------

# - basic doctor to dict
class doctor_display(webapp2.RequestHandler):
    def get(self):
        q = User.query(User.account_type == 'Doctor')
        l = []
        for n in q:
            ndict = n.to_dict()
            l.append(ndict)
            break
        self.response.write(json.dumps(l))


# - basic nurse to dict
class nurse_display(webapp2.RequestHandler):
    def get(self):
        q = User.query(User.account_type == 'Nurse')
        l = []
        for n in q:
            ndict = n.to_dict()
            l.append(ndict)
            break
        self.response.write(json.dumps(l))


# - Basic patient to-dict
class upatient_display(webapp2.RequestHandler):
    def get(self):
        q = User.query(User.account_type == 'Patient')
        l = []
        for n in q:
            ndict = n.to_dict()
            l.append(ndict)
            break
        self.response.write(json.dumps(l))


# - Testing what is displayed for patients. This was required to see
# - links and outputs because all patients have decendent class instances
class patient_display(webapp2.RequestHandler):
    def get (self):
        p = Patient.query()
        l = []
        for c in p:
            ndict = c.logindetails_userdict()
            l.append(ndict)
        self.response.write(json.dumps(l))

# - Checking login output.
# - This is using the Version 1 output
# - May update to beautiful version 2, Key retrieval, but I don't think the
# - Output changes, only the actual retrieval
class login_display(webapp2.RequestHandler):
    def get (self):
        req_email = 'jdorian@hospital'
        req_password = 'qwerty'
        _ukey = ndb.Key('User', req_email)
        _user = _ukey.get()

        if _user != None:
            if _user.password == req_password:
                user_dict = _user.logindetails_userdict()
                userper_list = []
                for cur_perm in _user.permissions:
                    userper_list.append(cur_perm.to_dict())

                user_dict['Permissions'] = userper_list
                self.response.write(json.dumps(user_dict))
        else:
            self.response.write(json.dumps({}))


class patient_info_display(webapp2.RequestHandler):
    def get(self):
        uk = ndb.Key('User', 'lwang@external')
        curs = Patient_Info.query(Patient_Info.patient==uk).fetch()
        for p in curs:
            self.response.write(p.to_dict())

# TODO: Update Delete user Test to try archive data
class delete_user_test(webapp2.RequestHandler):
    def get(self):
        _delete_key = Construct_User_Key('lwang@external')
        _d_user = _delete_key.get()
        if _d_user != None:
            if _d_user.account_type == 'Patient':
                curs = Patient_Info.query(Patient_Info.patient==_delete_key).fetch()
                for info in curs:
                    _delete_info = info2
                    _delete_info.key.delete()

            _d_user.key.delete()
            self.response.write('Successful Delete of User: lwang@external')
        else:
            self.response.write('Fail: Something went wrong    :(')
