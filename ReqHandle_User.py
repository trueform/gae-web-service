import urllib2
import cgi
import webapp2
import json
from google.appengine.ext import ndb
from google.appengine.ext.ndb import query
from datetime import date
# - Custom Modules
from User import User
from User import Construct_User_Key
from User import Patient_Info
from User import Archived_Info
from Permissions import Permission
import ReqHandle_Permission

class Create_User(webapp2.RequestHandler):
    def post(self):
        cr_obj = json.loads(self.request.body)
        cr_key = Construct_User_Key(cr_obj['email'])
        _validation = cr_key.get()
        if _validation == None:
            email = cr_obj['email']
            first_name = cr_obj['first_name']
            surname = cr_obj['surname']
            password = cr_obj['password']
            address = cr_obj['address']
            account_type = cr_obj['account_type']

            permission_list = []
            if account_type == 'Doctor':
                permission_list = ReqHandle_Permission.DEFAULT_DOCTOR()

            elif account_type == 'Nurse':
                permission_list = ReqHandle_Permission.DEFAULT_NURSE()

            elif account_type == 'Patient':
                permission_list = ReqHandle_Permission.DEFAULT_PATIENT()

            _user = User( key = cr_key,
                          first_name = surname,
                          surname = surname,
                          email = email,
                          address = address,
                          password = account_type,
                          account_type = account_type,
                          permissions = permission_list)
            _user.put()
            self.response.write('Success')
        else:
            self.response.write('Error')



    def get(self):
        c_email = self.request.headers['email']
        if c_email != '':
            # - Validates email address is unique
            cr_key = Construct_User_Key(c_email)
            Validation = cr_key.get()
            if Validation == None:
                # - Instanstiates the variables for the object
                c_first_name = self.request.headers['first_name']
                c_surname = self.request.headers['surname']
                c_password = self.request.headers['password']
                c_address = self.request.headers['address']
                c_account_type = self.request.headers['account_type']

                permission_list = []
                if c_account_type == 'Doctor':
                    permission_list = ReqHandle_Permission.DEFAULT_DOCTOR()

                elif c_account_type == 'Nurse':
                    permission_list = ReqHandle_Permission.DEFAULT_NURSE()

                elif c_account_type == 'Patient':
                    permission_list = ReqHandle_Permission.DEFAULT_PATIENT()

                c_user = User( key = cr_key,
                              first_name = c_surname,
                              surname = c_surname,
                              email = c_email,
                              address = c_address,
                              password = c_account_type,
                              account_type = c_account_type,
                              permissions = permission_list)
                c_user.put()
                self.response.write('Success')
            else:
                self.response.write('Validation Error')

class Change_User_Details(webapp2.RequestHandler):
    def post(self):
            _user = Construct_User_Key(self.request.headers['email']).get()
            if _user != None:
                _user.first_name = self.request.headers['first_name']
                _user.surname = self.request.headers['surname']
                _user.address = self.request.headers['address']
                _user.account_type = self.request.headers['account_type']
                _user.condition_status = self.request.headers['condition_status']
                _user.put()
                self.response.write('Details changed')
            else:
                self.response.write('Could not retrieve User')


class Create_Patient_Info(webapp2.RequestHandler):
    def post(self):
        _user_key = Construct_User_Key(self.request.headers['email'])
        if _user_key.get() != None:
            dobconv = datetime(self.request.headers['dob'])
            _PI = Patient_Info( patient = _user_key,
                                dob = dobconv,
                                age = self.request.headers['age'],
                                gender = self.request.headers['gender'],
                                height = self.request.headers['height'],
                                weight = self.request.headers['weight'],
                                bmi = self.request.headers['bmi'],
                                condition_status = self.request.headers['condition_status'])
            _PI.put()
            self.response.write('Successful')
        else:
            self.response.write('Error validating User')

# TODO: Test-Patient Info update

# TODO: Test Patient Info Archiving of previous versions
class Update_Patient_Info(webapp2.RequestHandler):
    def post(self):
        _user_key = Construct_User_Key(self.request.headers['email'])
        if _user_key.get() != None:
            _pi_edits = Patient_Info.query(Patient_Info.patient == _user_key)
            if _pi_edits.count() == 1:
                for _pi in _pi_edits:
                    _delete_info = _pi
                    Archive_Info(_delete_info)
                    _delete_info.key.delete()
                dobconv = datetime(self.request.headers['dob'])
                _updated_pi = Patient_Info( patient = _user_key,
                                dob = dobconv,
                                age = self.request.headers['age'],
                                gender = self.request.headers['gender'],
                                height = self.request.headers['height'],
                                weight = self.request.headers['weight'],
                                bmi = self.request.headers['bmi'],
                                condition_status = self.request.headers['condition_status'])
                _updated_pi.put()
                self.response.write('Successful')
            else:
                self.response.write('Error retrieving: Patient_Info')
        else:
            self.response.write('Error retrieving: User')


def Archive_Info(Old_Info):
    ai = Archived_Info(patient = Old_Info.patient,
                        dob = Old_Info.dob,
                        email = Old_Info.email,
                        gender = Old_Info.gender,
                        height = Old_Info.height,
                        weight = Old_Info.weight,
                        bmi = Old_Info.bmi,
                        condition_status = Old_Info.condition_status)
    ai.put()


class Delete_User(webapp2.RequestHandler):
    def get(self):
        _delete_key = Construct_User_Key(self.request.headers['email'])
        _d_user = _delete_key.get()
        if _d_user != None:
            if _d_user.account_type == 'Patient':
                curs = Patient_Info.query(Patient_Info.patient==_delete_key).fetch()
                for info in curs:
                    _delete_info = info
                    Archive_Info(_delete_info)
                    _delete_info.key.delete()

            _d_user.key.delete()
            self.response.write('Successful Delete of User:  lwang@external')#TODO: Revert self.request.headers['email'])
        else:
            self.response.write('Fail: Something went wrong    :(')

class Retrieve_ContactList(webapp2.RequestHandler):
    def get(self):
        # - Retrieves all user objects from datastore that are not the current user
        request_email = self.request.headers['email']
        _users = User.query(User.email != request_email)
        contact_list = []
        for current_user in _users:
            # - iterates through cursor to convert all user objects to dict using custom  to_dict
            current_userdict = current_user.contactlist_userdict()
            contact_list.append(current_userdict)

        self.response.write(json.dumps(contact_list))

#TODO: Test Change_LoginCredentials
class Change_LoginCredentials(webapp2.RequestHandler):
    def post(self):
        _user = Construct_User_Key(self.request.headers['email']).get()
        if _user != None:
            verif_password = self.request.headers['verif_password']
            new_password = self.request.headers['new_password']
            if verif_password == _user.password:
                _user.password = verif_password
                _user.put()
                self.response.write('Password Changed')
            else:
                self.response.write('Password Incorrect')
        else:
            self.response.write('Error Retrieving User')
