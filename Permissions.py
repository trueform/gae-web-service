from google.appengine.ext import ndb

#Super Permission Definition#
class Permission(ndb.Model):
	alias = ndb.StringProperty(required = True)

	def to_dict(self):
		return {'alias' : self.alias}


#Nitty Gritty Task Class#
class Task(ndb.Model):
	assigned_id = ndb.KeyProperty(kind ='User',required = True)
	assigned_email = ndb.StringProperty(required = True)
	category = ndb.StringProperty(required = True)
	patient_id = ndb.StringProperty(required = True)
	creator_id = ndb.StringProperty(required = True)
	date_assigned = ndb.DateTimeProperty(auto_now_add = True)
	task_info = ndb.StringProperty(required = True)
	task_status = ndb.StringProperty(choices = ['COMPLETE', 'INCOMPLETE'], default = 'INCOMPLETED')

	def to_dict(self):
		datestr = str(self.date_assigned)
		task_dict ={'assigned_email' : self.assigned_email,
					'category' : self.category,
					'patient_id' : self.patient_id,
					'date_assigned' : datestr,
					'task_info' : self.task_info,
					'task_status' : self.task_status}
		return task_dict


#User_Note class definition
class User_Note(ndb.Model):
	assigned_id = ndb.KeyProperty(kind = 'User',required = True)
	assigned_email = ndb.StringProperty(required = True)
	author_id = ndb.StringProperty()
	date_assigned = ndb.DateTimeProperty(auto_now_add = True)
	note = ndb.BlobProperty(required = True, indexed = False)

	def to_dict(self):
		datestr = str(self.date_assigned)
		un_dict = {	'assigned_email' : self.assigned_email,
					'author_id' : self.author_id,
					'date_assigned' : datestr,
					'note' : self.note}
		return un_dict
