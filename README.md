# README #

GAE Web Service is a python developed Service, that runs on Google's Cloud App Engine platform, and interacts with a Google Datastore Cloud Storage platform.
The hope is that this web service will manage all the storage and data retrievals necessary for the Remote Health ios app.

This repository is made to handle the python modules and models related to the retrieval and manipulation of our client side data.


*Developed by Trueform*

*Powered by Ericsson Australia, Latrobe University*


* Version: [2.0]
* Language: [Python 2.7]
* Hosting Platform: [Google Cloud Platforms]
* Lead Developer/ Contact: [Nick Freemantle]
* Development Team: [Trueform]
